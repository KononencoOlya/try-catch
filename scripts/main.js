const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];


const div = document.createElement("div");
div.id = 'root';
document.body.prepend(div);

 function booksList (arr) {
    const ul = document.createElement("ul");
    arr.forEach(element => {
      const li = document.createElement("li");
      li.textContent = `author: ${element.author}; name: ${element.name}; price: ${element.price}`;
      try{
        if(element.author && element.name && element.price) {
          ul.append(li);
        } else{
          throw new Error(`Отсутствует свойство: ${!element.author ? 'author' : !element.name ? 'name' : 'price'}`);
        }
      
       } catch (err) {
        console.log(err.message);
       }
    })
    return ul;
  }

  let list = booksList(books);
  div.append(list)

